from conans import ConanFile, CMake

class CxxProfConan(ConanFile):
    name = "cxxprof_preloader"
    version = "1.1.1"
    url = "https://gitlab.com/groups/CxxProf"
    license = "GNU LESSER GENERAL PUBLIC LICENSE Version 3"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    exports = "*"

    def config(self):
        self.requires.add("Pluma/1.1@monsdar/testing")

    def build(self):
        cmake = CMake(self.settings)
        self.run('cmake %s %s' % (self.conanfile_directory, cmake.command_line))
        self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.h", dst="include/cxxprof_preloader", src="%s/cxxprof_preloader" % self.conanfile_directory)
        self.copy("*.lib", dst="lib", src="lib")
        self.copy("*.dll", dst="bin", src="bin")
        self.copy("*.so", dst="bin", src="bin")
        self.copy("*.a", dst="lib", src="lib")

    def package_info(self):
        self.cpp_info.libs = ["cxxprof_preloader"]
        